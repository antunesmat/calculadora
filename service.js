/* Declaracao de variaveis */
var op = '';
var num1 = '';
var num2 = '';
var resultado = 0;
/*-------------------- Funcionalidades dos botoes e tela da calculadora -----------------------*/

function sinal(x){ /*Capturar sinal do usuario*/
    op = x;
    document.getElementById("recebe2").innerText=x;
}

function clean(){ /*Ac - limpar dados da tela e das variaveis*/
    document.getElementById("recebe").innerText='';
    document.getElementById("recebe2").innerText='';
    num1 = '';
    num2 = '';
    resultado = '';
    op = '';
}

function concatenar_num(x){
    if ((parseFloat(x)>=0 && parseFloat(x)<=9 || x=='.') && op==''){ /*Se for um digito informado e nao informamos o operador adicionamos ele em num1*/
        num1+=x;    /*Concatenar digito informado para apos converte-lo para int*/
        imprime(num1);      /*Imprimir na tela o numero completo informado*/
    }
    else if ((parseFloat(x)>=0 && parseFloat(x)<=9 || x=='.') && op!=''){ /*Se for um digito informa e ja informamos o operador adicionamos eke em num2*/
        num2+=x;
        imprime(num2);  
    }
    else{
        document.getElementById("recebe2").innerText=x;    /*Se nao for um digito (nesse caso um operador) imprimimos ele e nao concatenamos*/
    }
}

function imprime(x){ /*Imprimir valores*/
    document.getElementById("recebe").innerText=x;
}

/*-------------------- Operacoes matematica ----------------------*/

function operacao(x){
    num1=parseFloat(num1); /*Converter str para float*/
    num2=parseFloat(num2); /*Converter str para float*/
    if(op=='+'){
        resultado=soma(num1, num2);
    }
    else if(op=='-'){
        op='';
        resultado=subtracao(num1, num2);
    }
    else if(op=='X'){
        op='';
        resultado=multiplicacao(num1, num2);
    }
    else if(op=='/'){
        op='';
        resultado=divisao(num1, num2);
    }
    document.getElementById("recebe2").innerText=x;
    document.getElementById("recebe").innerText=resultado;
}

function soma(a, b){
    return a+b;
}
function subtracao(a, b){
    return a-b;
}
function multiplicacao(a, b){
    return a*b;
}
function divisao(a, b){
    if (b==0){ /*Se o divisor for 0 enviar mensagem de erro */
        return "Error 0"
    }
    else{
        return a/b;
    }
}